package models;

public class Review {
  private String profileName;
  private String productId;
  private String userId;
  private String helpfulness;
  private double score;
  private long time;
  private String summary;
  private String text;
  private String group;
  public String getProductId() {
    return productId;
  }

  public String getUserId() {
    return userId;
  }

  public String getProfileName() {
    return profileName;
  }

  public String getHelpfulness() {
    return helpfulness;
  }

  public double getScore() {
    return score;
  }

  public long getTime() {
    return time;
  }

  public String getSummary() {
    return summary;
  }

  public String getText() {
    return text;
  }

  public String getGroup() {
    return group;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }
  public void setProfileName(String profileName) {
    this.profileName = profileName;
  }

  public void setHelpfulness(String helpfulness) {
    this.helpfulness = helpfulness;
  }

  public void setScore(double score) {
    this.score = score;
  }

  public void setTime(long time) {
    this.time = time;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public void setText(String text) {
    this.text = text;
  }

  public void setGroup(String substring) {
    this.group = substring;
  }

  @Override
  public String toString() {
    return "models.Review{" +
      "productId='" + productId + '\'' +
      ", userId='" + userId + '\'' +
      ", profileName='" + profileName + '\'' +
      ", helpfulness='" + helpfulness + '\'' +
      ", score=" + score +
      ", time=" + time +
      ", summary='" + summary + '\'' +
      ", text='" + text + '\'' +
      '}';
  }
}




