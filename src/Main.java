import comparators.ProductIdComparator;
import models.Review;
import utils.AddReviewGroup;

import java.io.IOException;
import java.util.List;

public class Main {
  public static void main(String[] args) throws IOException {
    AddReviewGroup.addGroupsTOReviews();
    ReviewManager reviewManager = new ReviewManager();
    reviewManager.loadReviews("src/resources/foods-with-group.txt");
    ReviewHandler reviewHandler = new ReviewHandler();
    reviewHandler.sortByAmountOfReviews(reviewManager.getReviewsMap());
  }
}