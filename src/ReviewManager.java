import models.Review;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReviewManager {

  private Map<String, List<Review>> reviewsMap;

  public ReviewManager() {
    reviewsMap = new HashMap<>();
  }

  public Map<String, List<Review>> getReviewsMap() {
    return reviewsMap;
  }

  public void loadReviews(String filename) throws IOException {
    BufferedReader reader = new BufferedReader(new FileReader(filename));
    String line;
    Review review = null;

    while ((line = reader.readLine()) != null) {
      if (line.startsWith("product/productId:")) {
        String productId = line.substring(19);
        review = new Review();
        review.setProductId(productId);
      } else if (line.startsWith("review/userId:")) {
        review.setUserId(line.substring(15));
      } else if (line.startsWith("review/profileName:")) {
        review.setProfileName(line.substring(20));
      } else if (line.startsWith("review/helpfulness:")) {
        review.setHelpfulness(line.substring(20));
      } else if (line.startsWith("review/score:")) {
        review.setScore(Double.parseDouble(line.substring(14)));
      } else if (line.startsWith("review/time:")) {
        review.setTime(Long.parseLong(line.substring(13)));
      } else if (line.startsWith("review/summary:")) {
        review.setSummary(line.substring(16));
      } else if (line.startsWith("review/text:")) {
        review.setText(line.substring(13));
      } else if (line.startsWith("review/group:")) {
        String group = line.substring(14);
        review.setGroup(group);
        if (review != null) {
          reviewsMap.computeIfAbsent(group, k -> new ArrayList<>());
          reviewsMap.get(group).add(review);
        }
        review = null;
      }
    }
    reader.close();
  }

  public void saveReviewsToFile(String filename, Map<String, List<Review>> reviewMap) {
    try {
      PrintStream originalOut = System.out;  // Store original System.out
      PrintStream fileOut = new PrintStream(new File(filename));  // Create a file output stream
      System.setOut(fileOut);
      printReviews(reviewMap);
      System.setOut(originalOut);
      System.out.println("Reviews saved to " + filename);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }

  public void printReviews(Map<String, List<Review>> reviewMap) {
    for (Map.Entry<String, List<Review>> entry : reviewMap.entrySet()) {
      String group = entry.getKey();
      List<Review> reviews = entry.getValue();

      for (Review review : reviews) {
        System.out.println("review/group: " + group);
        System.out.println("review/profileName: " + review.getProfileName());
        System.out.println("review/helpfulness: " + review.getHelpfulness());
        System.out.println("review/score: " + review.getScore());
        System.out.println("review/time: " + review.getTime());
        System.out.println("review/summary: " + review.getSummary());
        System.out.println("review/text: " + review.getText());
        System.out.println("review/productId: " + review.getProductId());  // Add this line
        System.out.println();
      }
    }
  }

  public List<Review> getReviewsByGroup(String a) {
    return reviewsMap.get(a);
  }

  public void saveReviewsToTextFile(List<Review> reviews, String s) {
try {
      PrintStream originalOut = System.out;  // Store original System.out
      PrintStream fileOut = new PrintStream(new File(s));  // Create a file output stream
      System.setOut(fileOut);
      printReviewsList(reviews);
      System.setOut(originalOut);
      System.out.println("Reviews saved to " + s);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }

  private void printReviewsList(List<Review> reviews) {
    for (Review review : reviews) {
      System.out.println("review/productId: " + review.getProductId());  // Add this line
      System.out.println("review/userId: " + review.getUserId());
      System.out.println("review/profileName: " + review.getProfileName());
      System.out.println("review/helpfulness: " + review.getHelpfulness());
      System.out.println("review/score: " + review.getScore());
      System.out.println("review/time: " + review.getTime());
      System.out.println("review/summary: " + review.getSummary());
      System.out.println("review/text: " + review.getText());
      System.out.println("review/group: " + review.getGroup());
      System.out.println();
    }
  }
}
