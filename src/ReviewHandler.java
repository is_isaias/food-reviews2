import comparators.ProductIdComparator;
import comparators.UserIDComparator;
import models.Review;

import java.util.*;

public class ReviewHandler {
  public void sortReviewsByProductId(Map<String, List<Review>> reviewsMap) {
    List<Review> allReviews = new ArrayList<>();
    for (List<Review> reviews : reviewsMap.values()) {
      allReviews.addAll(reviews);
    }
    ReviewSorter sorter = new ReviewSorter();
    sorter.mergeSort(allReviews, 0, allReviews.size() - 1, new ProductIdComparator());
    reviewsMap.clear();
    for (Review review : allReviews) {
      reviewsMap.computeIfAbsent(review.getGroup(), k -> new ArrayList<>()).add(review);
    }
  }

  public List<Review> searchByGroupAndUserId(Map<String, List<Review>> reviewsMap, String userId, String group) {
    List<Review> reviewsByGroup = new ArrayList<>();
    reviewsByGroup.addAll(reviewsMap.get(group));
    if (reviewsByGroup.isEmpty()) {
      System.out.println("No reviews found for group " + group);
      return null;
    } else {
      ReviewSorter sorter = new ReviewSorter();
      sorter.mergeSort(reviewsByGroup, 0, reviewsByGroup.size() - 1, new UserIDComparator());
      ReviewSearcher searcher = new ReviewSearcher();
      List<Review> matchingReviews = searcher.binarySearchByUserId(reviewsByGroup, userId);
      if (matchingReviews.isEmpty()) {
        System.out.println("No reviews found for user " + userId);
        return null;
      } else {
        return matchingReviews;
      }
    }
  }

  public Map<String, Integer> sortByAmountOfReviews(Map<String, List<Review>> reviewMap) {
    Map<String, Integer> sortedMap = new LinkedHashMap<>();
    List<Map.Entry<String, List<Review>>> sortedEntries = new ArrayList<>(reviewMap.entrySet());
    sortedEntries.sort((entry1, entry2) -> Integer.compare(entry2.getValue().size(), entry1.getValue().size()));
    for (Map.Entry<String, List<Review>> entry : sortedEntries) {
      sortedMap.put(entry.getKey(), entry.getValue().size());
    }
    for (Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
      System.out.println("Grupo: " + entry.getKey() + ", Cantidad de Reviews: " + entry.getValue());
    }
    return sortedMap;
  }
}
