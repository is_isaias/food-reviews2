package utils;

import java.io.*;

public class AddReviewGroup {
  public static void addGroupsTOReviews( ) {
      String inputFile = "src/resources/foods.txt";
      String outputFile = "src/resources/foods-with-group.txt";

      String[] newGroupValues = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};

      try {
        BufferedReader reader = new BufferedReader(new FileReader(inputFile));
        BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));

        String line;
        StringBuilder entryBuilder = new StringBuilder();

        while ((line = reader.readLine()) != null) {
          if (line.isEmpty()) {
            String group = newGroupValues[(int) (Math.random() * newGroupValues.length)];
            entryBuilder.append("review/group: ").append(group).append("\n\n");
            writer.write(entryBuilder.toString());
            entryBuilder.setLength(0);
          } else {
            entryBuilder.append(line).append("\n");
          }
        }
        reader.close();
        writer.close();
        System.out.println("Review groups added successfully.");
      } catch (IOException e) {
        System.err.println("An error occurred: " + e.getMessage());
      }
  }
}
