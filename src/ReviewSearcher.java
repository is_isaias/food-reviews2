import comparators.UserIDComparator;
import models.Review;

import java.util.ArrayList;
import java.util.List;

public class ReviewSearcher {
  public  List<Review> binarySearchByUserId(List<Review> reviews, String userId) {
    int left = 0;
    int right = reviews.size() - 1;
    List<Review> matchingReviews = new ArrayList<>();
    while (left <= right) {
      int mid = left + (right - left) / 2;
      Review midReview = reviews.get(mid);
      int comparison = midReview.getUserId().compareTo(userId);
      if (comparison == 0) {
        matchingReviews.add(midReview);
        int leftIndex = mid - 1;
        while (leftIndex >= 0 && reviews.get(leftIndex).getUserId().equals(userId)) {
          matchingReviews.add(reviews.get(leftIndex));
          leftIndex--;
        }
        int rightIndex = mid + 1;
        while (rightIndex < reviews.size() && reviews.get(rightIndex).getUserId().equals(userId)) {
          matchingReviews.add(reviews.get(rightIndex));
          rightIndex++;
        }
        return matchingReviews;
      } else if (comparison < 0) {
        left = mid + 1;
      } else {
        right = mid - 1;
      }
    }
    return matchingReviews;
  }

}
