import models.Review;

import java.util.*;

import comparators.GroupReviewComparator;

public class ReviewSorterGroupsByReview {
  public List<String> getGroupsSortedByMostReviews(Map<String, List<Review>> reviewsMap) {
    Map<String, Integer> groupReviewCounts = new HashMap<>();
    for (List<Review> reviews : reviewsMap.values()) {
      String group = reviews.get(0).getGroup();
      groupReviewCounts.put(group, groupReviewCounts.getOrDefault(group, 0) + reviews.size());
    }
    List<Map.Entry<String, Integer>> groupReviewCountList = new ArrayList<>(groupReviewCounts.entrySet());
    Collections.sort(groupReviewCountList, new GroupReviewComparator());
    List<String> sortedGroups = new ArrayList<>();
    for (Map.Entry<String, Integer> entry : groupReviewCountList) {
      sortedGroups.add(entry.getKey());
    }
    return sortedGroups;
  }

}
