
import models.Review;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ReviewSorter {
  public void mergeSort(List<Review> reviews, int l, int r, Comparator<Review> comparator) {
    if (l < r) {
      int m = l + (r - l) / 2;
      mergeSort(reviews, l, m, comparator);
      mergeSort(reviews, m + 1, r, comparator);
      merge(reviews, l, m, r, comparator);
    }
  }

  private void merge(List<Review> reviews, int l, int m, int r, Comparator<Review> comparator) {
    int n1 = m - l + 1;
    int n2 = r - m;

    List<Review> left = new ArrayList<>(n1);
    List<Review> right = new ArrayList<>(n2);

    for (int i = 0; i < n1; ++i) {
      left.add(reviews.get(l + i));
    }
    for (int j = 0; j < n2; ++j) {
      right.add(reviews.get(m + 1 + j));
    }

    int i = 0, j = 0, k = l;
    while (i < n1 && j < n2) {
      if (comparator.compare(left.get(i), right.get(j)) <= 0) {
        reviews.set(k, left.get(i));
        i++;
      } else {
        reviews.set(k, right.get(j));
        j++;
      }
      k++;
    }

    while (i < n1) {
      reviews.set(k, left.get(i));
      i++;
      k++;
    }

    while (j < n2) {
      reviews.set(k, right.get(j));
      j++;
      k++;
    }
  }


}
