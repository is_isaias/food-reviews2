package comparators;

import models.Review;
import java.util.Comparator;

public class UserIDComparator implements Comparator<Review> {
  @Override
  public int compare(Review o1, Review o2) {
    return o1.getUserId().compareTo(o2.getUserId());
  }
}
