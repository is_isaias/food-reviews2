package comparators;

import java.util.Comparator;
import models.Review;
public class ProductIdComparator implements Comparator<Review> {
  @Override
  public int compare(Review o1, Review o2) {
    return o1.getProductId().compareTo(o2.getProductId());
  }
}
