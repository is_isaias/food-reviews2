package comparators;

import java.util.Comparator;
import java.util.Map;

public class GroupReviewComparator implements Comparator<Map.Entry<String, Integer>> {
  @Override
  public int compare(Map.Entry<String, Integer> entry1, Map.Entry<String, Integer> entry2) {
    return Integer.compare(entry2.getValue(), entry1.getValue());
  }
}
